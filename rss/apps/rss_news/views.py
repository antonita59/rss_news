from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from rss.apps.rss_news.models import Post

# Create your views here.


def index(request):

    posts = reversed(Post.objects.all())

    context = {
        'posts': posts
    }

    return render(request, "index.html", context)


def post(request, index):

    try:
        post = Post.objects.get(id=index)
        previous_id = post.id - 1
        next_id = post.id + 1
        context = {
            'post': post,
            'previous_id': previous_id,
            'next_id': next_id,
            'max_id': len(Post.objects.all())
        }
        return render(request, "post.html", context)
    except Post.DoesNotExist:
        return render(request, "error_page.html")

def login(request):
    return render(request, "login.html")


def signup(request):
    return render(request, "signup.html")


def signup_user(request):

    if request.method == "POST":
        user_name = request.POST["user_name"]
        password = request.POST["password"]
        repeat_password = request.POST["repeat_password"]
        email = request.POST["email"]

        print(user_name, password, repeat_password, email)

        return HttpResponseRedirect("/")
    else:
        return render(request, "error_page.html")


def check_user_name(request):

    if request.method == "GET":
        user_name = request.GET["user_name"]
        names = {"Katya", "Igor", "Maxim", "Anna"}

        if user_name in names:
            return HttpResponse("no", content_type='text/html')
        else:
            return HttpResponse("ok", content_type='text/html')

    else:
        return HttpResponse("no", content_type='text/html')
